<?php
/**
  *
  * Styles to Load
  *
  * How to use:
  *
  * Copy your styles files to assets/style
  * 
  * 1) Loading styles in all pages
  * $styles = [
  *             ['style_name', 'style1.css']
  *             ['style_name2', 'style2.css']
  *           ];
  *
  * 2) Loading on specifi pages
  * $styles = ['admin' => ['style_name', 'style1.css']]; (Only will load on Admin page)
  * $styles = ['user'  => ['style_name2', 'style2.css']]; (Only will load on user page (Site front page))
  *
  * 3) Combine them to load diferent styles in different parts
  *
  *	$styles = [
  *				'both' 	=> ['style', 'style.css'],
  *				'admin' => ['style_admin', 'style_admin.css'],
  *				'user' 	=> ['style_user', 'style_user.css']
  *				];
  *
  *  This code will load style.css in all pages, style_admin.css on admin pages and
  *  style_user.css in user pages
  *
  * 4) To load external styles, include all path
  * 
  * $styles = [
  *             ['style_name', 'style1.css']
  *             ['bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css']
  *           ];
  *
  * 5) If you want to add more info (https://developer.wordpress.org/reference/functions/wp_enqueue_style/) to your style:
  *
  *	$styles = [where => [handler, src, dependencies, version, media]];
  * 
  **/

	$styles = [
               'admin'  => [
                            ['plugin_slug_cboiler', 'plugin_slug_cboiler.css', array(), false, false, false], //Basic style to be used on Admin area
                            ['plugin_slug_admin_style', 'plugin_slug_style_admin.css', array(), false, false, false]
                           ],
			   'both'   => ['plugin_slug_both_style', 'plugin_slug_style.css', array(), false, false, false],
			   'user' 	=> ['plugin_slug_user_style', 'plugin_slug_style_user.css', array(), false, false, false],
			  ];
