<div class="wrap">
    <h2><?=_e('Plugin_Name Page', PLUGINDEFINE_LANG);?> 1</h2>
    <p><?=_e('About this page', PLUGINDEFINE_LANG);?> (1)</p>
    <div class="cboiler-box">
        <?=$helper->form()->open(['class' => 'cboiler-form']);?>
            <fieldset>
                <section>
                    <?=$helper->form()->label('@.text', __('Text', PLUGINDEFINE_LANG).':', ['class' => 'label']);?>
                    <label class="input">
                        <?=$helper->form()->text('@.text', $helper->get('text'), ['id' => 'text', 'placeholder' => __('Text', PLUGINDEFINE_LANG)]);?>
                    </label>
                    <div class="note">
                        <?=_e('Current Value', PLUGINDEFINE_LANG);?>: <strong><?=$helper->form()->name('@.text');?></strong> => <?=$helper->get('text');?>
                    </div>
                </section>
            </fieldset>
            <footer>
                <?=$helper->form()->submit(__('Save Options', PLUGINDEFINE_LANG), ['class' => 'btn btn-green']);?>
            </footer>
            
        <?=$helper->form()->close();?>
    </div>
</div>