<?php
namespace PluginNameSpace\Core\Lib;

use PluginNameSpace\Core\Lib\Load;

class Assets extends General {
	
	function __construct() {
		add_action( 'wp_enqueue_scripts',      array($this, 'load_scripts') );
		add_action( 'admin_enqueue_scripts',   array($this, 'load_scripts') );
		add_action( 'wp_enqueue_scripts',      array($this, 'load_styles') );
		add_action( 'admin_enqueue_scripts',   array($this, 'load_styles') );
	}

	/* Call WP function to load scripts
	 * 
     * @return void
     *
     */
    public function load_scripts() {
		self::load_assets('scripts');
	}
	
	/* Call WP function to load styles
	 * 
     * @return void
     *
     */
    public function load_styles() {
		self::load_assets('styles');
	}
	
	/* Call WP function to load assets
	 * 
     * @param $option string (Which asset will load scripts or styles)
     *
     * @return void
     *
     */
    public function load_assets($option) {
		$assets = Load::config($option);
		foreach ($assets as $key => $asset) {			
			if ($key === 'admin') {
				if (is_admin()) self::wp_asset($asset, $option);
			} elseif ($key === 'user') {
				if (!is_admin()) self::wp_asset($asset, $option);
			} elseif ($key === 'both') self::wp_asset($asset, $option);
			else self::wp_asset($script, $option);
		}
	}
	
	/* Call WP function to register and enqueue an asset
	 * 
     * @param $asset array
     * @param $option string
     *
     * @return void
     *
     */
	private function wp_asset($assets, $option) {
		
		if (!isset($assets[0]) && !isset($assets[1])) return;
		
		if (is_array($assets[0]))
			foreach ($assets as $asset) $this->do_asset_load($asset, $option);
		else $this->do_asset_load($assets, $option);
		
	}
	
	/* Partial used on wp_asset
	 * 
     * @param $asset array
     * @param $option string
     *
     * @return void
     *
     */
	function do_asset_load($asset, $option) {
		$handle = $asset[0];
		$src = ($option == 'scripts')?Load::script($asset[1]):Load::style($asset[1]);
		$deps = (isset($asset[2]) && is_array($asset[2]))?$asset[2]:array();
		$ver = (isset($asset[3]))?$asset[3]:false;
		$last = (isset($asset[4]))?$asset[4]:false; //for script it is in_footer, for style it is media
		
		if ($option == 'scripts') {
			wp_register_script( $handle, $src, $deps, $ver, $last);
			wp_enqueue_script( $handle );
		} else {
			wp_register_style( $handle, $src, $deps, $ver, $last);
			wp_enqueue_style( $handle );			
		}
	}
	
	/* Get full url to image 
	 * 
     * @param $file string
     *
     * @return string|false (return false if the file doen't exist)
     *
     */
	
	public function get_img($file, $echo = true) {
		$img_path = DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . $file;
		
		$return = (file_exists(PLUGINDEFINE_PATH . $img_path) ? PLUGINDEFINE_URL . $img_path : false );
		
		if ($echo) echo $return;
		else return $return;
	}
	
}

