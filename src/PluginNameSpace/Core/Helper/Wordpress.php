<?php
namespace PluginNameSpace\Core\Helper;

use PluginNameSpace\Core\Lib\Page;

class Wordpress extends Helper {
	
	/* Get Attribute from a Class
	 *
	 * @param $class string
	 * @param $name string (if false, will return an array)
	 * 
     * @return string|array
     *
     */
	public function get_option($class, $name = false) {
		
		$page = new Page();
		
		$opt_values = $page->get_opt_values($class);
		$og_options = $page->get_page_data($opt_values['name']);
		
		if (!$name) return $og_options;
		
		$parts = explode('.', $name);
		
		if (!is_array($parts) || count($parts) === 1) return $og_options[$name];
		
		$value = $name;
		
		foreach ($parts as $part) {
			if (isset($value[$part]))
				$value = $value[$part];
		}
		
		return $value;
	
	}	
	
}
