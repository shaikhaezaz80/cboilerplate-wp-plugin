<?php
/*
* Plugin Name: Plugin_Name
* Plugin URI: http://website.com/
* Description: PLUGIN DESCRIPTION
* Author: You (I suppose)
* Version: 1.0.0
* License: GPL-2.0+
* Text Domain: plugin_slug_domain
* Domain Path: /languages
*/

use PluginNameSpace\Core\Lib\Plugin;

//Check if Absolute Path is defined
defined( 'ABSPATH' ) or die( 'At least you\'ve tried! It\'s something! I think!' );

defined('PLUGINDEFINE_NAMESPACE') or define('PLUGINDEFINE_NAMESPACE' , 'PluginNameSpace');
defined('PLUGINDEFINE_NAME')      or define('PLUGINDEFINE_NAME'      , 'plugin_slug');
defined('PLUGINDEFINE_PATH')      or define('PLUGINDEFINE_PATH'      , realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR);
defined('PLUGINDEFINE_URL')       or define('PLUGINDEFINE_URL'       , plugins_url( '',  __FILE__ ) );
defined('PLUGINDEFINE_BASENAME')  or define('PLUGINDEFINE_BASENAME'  , basename( dirname( __FILE__ ) ) );
defined('PLUGINDEFINE_LANG')      or define('PLUGINDEFINE_LANG'      , 'plugin_slug' );

spl_autoload_register( 'plugin_slug_autoloader' ); // Register autoloader
function plugin_slug_autoloader( $class_name ) {
    if ( false !== strpos( $class_name, 'PluginNameSpace' ) ) {
        $classes_dir = PLUGINDEFINE_PATH . 'src' . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        require_once $classes_dir . $class_file;
    }
}

add_action( 'plugins_loaded', 'plugin_slug_init' ); // Hook initialization function
function plugin_slug_init() {
	$plugin = new Plugin();
    $plugin->call_translation();
	$plugin->run();
}
