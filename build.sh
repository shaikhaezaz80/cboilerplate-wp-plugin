#!/bin/sh
# Argument = -n namespace -s slug

REPLACENAMESPACE="PluginNameSpace"
REPLACESLUG="plugin_slug"
REPLACENAME="Plugin_Name"
REPLACEDEFINE="PLUGINDEFINE"

# Color to be used on output
WHITE='\033[0;37m'
RED='\033[0;31m'
GREEN='\033[1;32m'
YELLOW='\033[0;33m'
NO_COLOR='\033[0m'

usage()
{
printf "${WHITE}"
cat << EOF
usage: $0 options

OPTIONS:
   -h      Show this message
   -n      The namespace of the plugin
   -s      The slug of the plugin
   -p      The name of the plugin
   -d      The prefix of define variables
   
USE: ./build.sh -n PluginName -s plugin_name -p "Plugin Name" -d PLUGINNAME

EOF
printf "${NO_COLOR}"
}

NAMESPACE=
DEFINE=
SLUG=
NAME=
while getopts "hn:s:p:d:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         n)
             NAMESPACE=$OPTARG
             ;;
         d)
             DEFINE=$OPTARG
             ;;
         s)
             SLUG=$OPTARG
             ;;
         p)
             NAME=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $NAMESPACE ]] || [[ -z $SLUG ]] || [[ -z $NAME ]] || [[ -z $DEFINE ]]
then
     usage
     exit 1
fi

printf "\n\n${WHITE}"
printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] ${WHITE}Starting...${WHITE}\n\n"

# Rename all directories and files
printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Renaming folder src to $NAMESPACE"
find . -depth -name '*PluginNameSpace*' -execdir bash -c 'mv -i "$1" "${1/$2/$3}"' bash {} $REPLACENAMESPACE $NAMESPACE \;
printf " [${GREEN}Done${WHITE}]\n"

printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Renaming file plugin_slug to $SLUG"
find . -depth -name '*plugin_slug*' -execdir bash -c 'mv -i $1 ${1/$2/$3}' bash {} $REPLACESLUG $SLUG \;
printf " [${GREEN}Done${WHITE}]\n"

# # Rename all instances inside of files.
printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Changing text on files (from $REPLACENAMESPACE to $NAMESPACE)"
find . -depth -type f \( -name "*.php" -o -name "*.xml" \) -execdir sed -i "" "s/$REPLACENAMESPACE/$NAMESPACE/g" {} \;
printf " [${GREEN}Done${WHITE}]\n"

printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Changing text on files (from $REPLACESLUG to $SLUG)"
find . -depth -type f \( -name "*.php" -o -name "*.xml" \) -execdir sed -i "" "s/$REPLACESLUG/$SLUG/g" {} \;
printf " [${GREEN}Done${WHITE}]\n"

printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Changing text on files (from $REPLACENAME to $NAME)"
find . -depth -type f \( -name "*.php" -o -name "*.xml" \) -execdir sed -i "" "s/$REPLACENAME/$NAME/g" {} \;
printf " [${GREEN}Done${WHITE}]\n"

printf "[${YELLOW}$(date '+%d/%m/%Y %H:%M:%S')${WHITE}] Changing text on files (from $REPLACEDEFINE to $DEFINE)"
find . -depth -type f \( -name "*.php" -o -name "*.xml" \) -execdir sed -i "" "s/$REPLACEDEFINE/$DEFINE/g" {} \;
printf " [${GREEN}Done${WHITE}]\n"

printf "\n\n"
printf "${GREEN}Everything done!${WHITE}\n\n"
printf "Any idea to improve it or do you want to report a bug?\n"
printf "   mauro.baptista@carnou.com or @carnou on twitter\n\n"
printf "Do you want to improve it by yourself?\n"
printf "   https://bitbucket.org/benjatech/cboilerplate-wp-plugin\n\n\n"
printf "${RED}Thanks to use cBoilerPlate WP Plugin${NO_COLOR}\n\n"